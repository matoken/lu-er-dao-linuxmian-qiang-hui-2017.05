<!-- $theme: default>
<!-- page_number: true -->
<!-- $size: 4:3 -->
<!-- footer: @matoken -->


# GNU socialあれこれ

##### KenichiroMATOHARA([@matoken](https://twitter.com/matoken/))
##### http://matoken.org 
### [鹿児島Linux勉強会2017.05(2017-05-13)](https://kagolug.connpass.com/event/55577/ )
### [モノづくりスペース TUKUDDO](http://tukuddo.com/ "モノづくりスペース TUKUDDO")
---

# [KenichiroMATOHARA](http://matoken.org)( [@matoken](https://twitter.com/matoken) )
* PC-UNIX/OSS, OpenStreetMap, 電子工作, 自転車……
* altanativeが好き，多様性は正義!

![](osmo.jpg)

---

# GNU social🐮

* 旧StatusNet(June 8th, 2013〜)
* 脱中央集権を目指したTwitter的なマイクロブログの実装
* OStatusプロトコルで各サーバと連合が組める
  * リモートフォローで別のサーバのユーザをフォローして購読したりメッセージのやり取りが可能
* つぶやき -> ノーティス(Qvitter pluginだとクイップ)
* 投稿文字数は無制限まで設定可能でサーバの設定による
* PHP製/GNU AGPLv3
* StatusNetは2009年ごろ試してみてた
  * [Twitter Clone なStatusNet を入れてみる - matoken’s meme -hatena-](http://d.hatena.ne.jp/matoken/20090903/1251994078 "Twitter Clone なStatusNet を入れてみる - matoken’s meme -hatena-")
    <!-- * liconi.ca / identi.ca / pump.io とかとか
    -->

---

# ホストされているサービスで試す

* [List of Independent GNU social Instances - I ask questions](http://skilledtests.com/wiki/List_of_Independent_GNU_social_Instances "List of Independent GNU social Instances - I ask questions") (久々に見るとすごい増えてる!)
* 管理者やライセンス，設定などで選ぶ
* 主にLoadaverage.orgを使ってみていた(最近管理が[Highland Arrow admin](https://community.highlandarrow.com/maiyannah)に)
* 日本人居ない問題
  * 日本語で投稿していいものだろうか……とか
    (最近はMastodon効果かサーバによっては日本語増えている?)

---

# Mastodon🐘が流行る

* 日本だけ急に来た!?
* きっかけは多分ホーテンス遠藤さんの記事
  [![150%](gs_01.jpg)](https://mastodon.social/@matoken/2212058)
* 今度は日本人多すぎ問題……．

---

[![85% center](gs_02.jpg)](http://ascii.jp/elem/000/001/465/1465842/)
[ASCII.jp：Twitterのライバル？　実は、新しい「マストドン」（Mastodon）とは！｜遠藤諭のプログラミング＋日記](http://ascii.jp/elem/000/001/465/1465842/ "ASCII.jp：Twitterのライバル？　実は、新しい「マストドン」（Mastodon）とは！｜遠藤諭のプログラミング＋日記")

---

# ちなみにMastodonを知った時はGNU socialの皮だと思っていた……．

![150%](gs_03.jpg)

---

# GNU socialには見た目を変えるプラグインがある

* Quitter
  TwitterのようなUIに
  https://quitter.se/ とかあちこちで採用
* [hannes / Qvitter · GitLab](https://git.gnu.io/h2p/Qvitter "hannes / Qvitter · GitLab")
  写真やイラストに特化したUI
  つい最近GutHubからGitLabに移動:smile:
  https://quit.im などで採用

---

![200%](gs_04.jpg)

---

![150%](Quitter.se.jpg)

---

![150%](quit.im.jpg)

---

# Mastodon🐘?

* OStatus互換(GNU social等と相互連携可能)
* TweetdeckぽいUI
* サーバ -> インスタンス
* つぶやき -> トゥート
* 1投稿500文字まで
* 投稿時にNSFW/CWが使える(NSFWは[GNU socialで実装しているところもある](https://gs.smuglo.li/))
* RoR / React.js

---

![150% center](mastodon.jpg)

---

![60%](nsfw01.jpg)![60%](nsfw02.jpg)

![60%](cw01.jpg)![60%](cw02.jpg)

---

# タイムラインは3種類

* ホームタイムライン
* ローカルタイムライン
* 連合タイムライン

---

# 色んな色のインスタンスがある

* [カテゴリ：[全て] - Mastodon Instance List(マストドン インスタンス リスト)](http://mastodon-instance.com/ "カテゴリ：[全て] - Mastodon Instance List(マストドン インスタンス リスト)")
* [documentation/List-of-Mastodon-instances.md at master · tootsuite/documentation](https://github.com/tootsuite/documentation/blob/master/Using-Mastodon/List-of-Mastodon-instances.md "documentation/List-of-Mastodon-instances.md at master · tootsuite/documentation")
* 信頼できるところにメインアカウント
* 更にペルソナごとにアカウント?
* （脱中央集権なのに一箇所に集まりすぎ問題……．）

---

# このインスタンスはどんな雰囲気だろう?

* GNU socialは普通にローカルタイムラインが閲覧できるが，Mastodonでは見えない(個別ユーザのTLは見える)
* アカウントを取らなくてもAPIで覗ける
  * `$ curl -sS 'https://mstdn.jp/api/v1/timelines/public' | jq -r ".[] | .account.display_name,.account.acct,.content" | lynx -stdin`
* TLを見るサービスも
  * [Mastodonのパブリックタイムラインを見るやつ](http://junk.azyobuzi.net/mastodonptl/ "Mastodonのパブリックタイムラインを見るやつ")

---

# Mastodonの導入を試す

* 自分だけの利用分だけでなく連合のデータも流れてくる&溜まるので従量制ではちょっと怖い
* 今契約しているVPS(定額)では容量的に難しかった
* 自宅サーバで立ててみるちょっと重い……．
* 使い方にもよるがディスクも結構食う
  * 自分だけでもイベントやお出かけがあると1日で1GBくらいSNSに写真をあげている
  * 週末だけとしても1年で50GB, 10人で500GB, 10年で5TB
* お財布的に維持できる気がしない
* 添付ファイルをatimeで見て古いものをNFS mountした自宅サーバに置いたりしてみたり……．
  アクセスがまたあると戻す?(未実装)

---

# hosting serviceも出てきている

* €5/month〜とか安いところも
  * [Masto.Host - Hosting for Mastodon Instances](https://masto.host/ "Masto.Host - Hosting for Mastodon Instances")
  €5/monthでこのサービス
    * Unmetered Bandwidth
    * Unlimited Disk Space
    * Up to 100 users
  * (ここで1post/sとかやってたインスタンスはbanされたらしい)

---

# GNU socialはMastodonに比べると軽い

* Raspberry Pi 1 B+でも一応動いている(ARMv6 700MHz/RAM 512MB/Diskは外部の2.5"HDD)
* リモートフォローしてもらえば流行りのMastodonで購読できる(Atom/RSS Feedも)
* 今契約しているVPSではPHPやMySQLも既に動しているので導入も簡単
* とりあえず主にアナウンス&MLメンバー限定(MLのリスト参加者じゃないと登録を蹴る)でVPS(さくらのVPS v3/1GB)のkagolug.orgで立ち上げてみる
* ディスクの問題はとりあえずMLメンバー限定&様子を見ながらQuotaで絞る感じで

---

# [gnusocial.kagolug.org](https://gnusocial.kagolug.org):cow:

* 主なアカウント
  * [@info@gnuscial.kagolug.org](https://gnusocial.kagolug.org/info)	アナウンス用
  * [@ml@gnuscial.kagolug.org](https://gnusocial.kagolug.org/ml)	ML通知
  * [@tokaidolug@gnuscial.kagolug.org](https://gnusocial.kagolug.org/tokaidolug) Twitterのハッシュタグ「#東海道らぐ」のGW
  * [@kagoluggw@gnuscial.kagolug.org](https://gnusocial.kagolug.org/kagoluggw) Twitterのハッシュタグ「#kagolug」のGW(夕べ作った)

---

# [gnusocial.kagolug.org](https://gnusocial.kagolug.org):cow: 04/23にアナウンス

* 結局利用者0なのででぃすくへらないよやったー
* アナウンスが主目的だし……
  * followerも0じゃないですかー

---

# Mastodonでのremote follow例

* 左上の検索ボックスで検索してfollow
  ![120%](remotefollow01.jpg)![120%](remotefollow02.jpg)
  ![](remotefollow03.jpg)

---

# 自分用に自宅サーバのものも公開

* [@matoken@gnusocial.matoken.org](https://gnusocial.matoken.org/matoken)
* 色々いじってるので止まったりも……．
* 写真は写真の保存ストレージと同じファイルシステムに置いてdedup(同じファイルが複数あっても1つ分の容量)してみている
  * もともと写真のバックアップもしているので増え方はあまり変わらない(サムネイルのみ)はず

---

# Raspberry Pi等のSBCでの運用

* [FreedomBox](https://www.freedomboxfoundation.org/) + GNU social + [ngrok](ngrok.com) とかの組み合わせならLinux知らない人でもどうにか運用できそう?
  * FreedomBoxは基本的にWEBベースで管理できる
  * GNU social導入が一番ハードルが高い?(WordPressが入れられるレベルの人ならほぼ同じ手順なので行けると思う)
* Diskは別にSSDやHDDを用意してこまめにバックアップを撮るようにしたほうが良い
* Raspberry Pi 1B+では503が多発するのでCPUが速くてRAMの多い2B以降のほうが良さそう
* 家庭に1台FreedomBox!

---

# GNU socialでつまずいたこととか

* Install scriptで「`Database error: DB Error: unknown error`」(Ubuntu 16.04で発生，Debian jessie/sidでは起こらなかった)
  * MySQLの`NO_ZERO_DATE`に引っかかっていた
  * 未定義や`0000-00-00 00:00:00`ではNGで明示的に`1970-01-01 00:00:01`な感じにしておかないといけないぽい
    [MySQL :: MySQL 5.7 Reference Manual :: 6.1.8 Server SQL Modes](https://dev.mysql.com/doc/refman/5.7/en/sql-mode.html#sqlmode_no_zero_date "MySQL :: MySQL 5.7 Reference Manual :: 6.1.8 Server SQL Modes")

---

* dbを利用するpluginを有効にして「DB_DataObject error[]: DB Error: no such table」
  * pluginを有効にした後`scripts/checkschema.php`を実行してtableを作成
```
$ sudo -u www-data scripts/checkschema.php

Table 'qvitternotification' not created yet, so nothing 
to do with it before Schema Update... DONE.
```

---

* Twitterと連携するpluginの`TwitterBridge`を有効にするときに
  * Twitterのaplication登録にはいつの間にか登録には電話番号が必須になっている
    例によって登録後削除は行けた
  * 取得したkey/secretで認証が通らない
    log[*1]を確認すると401の後のレスポンスがこんな`Desktop applications only support the oauth_callback value 'oob'`
    oauth_callbackでちゃんと投げているのにoob?
    Twitter aplicationの登録でCallBack URLが必要だった


\*1) logはsyslogに`statusnet`で出てくる

---

* Mastodonのユーザをremote followしようとして弾かれる
  * 該当ユーザのアカウント名の中に`_`が含まれている
  * GNU socialのアカウントは英数のみのようなのでそのせいかも?(`_`を含むGNU socialアカウントを作ってみたら`_`が無視された`hoge_fuga` -> `hogefuga`)
  * もうすこし追いかけてみる予定

---

# これから

* GNU socialにはTwitterのStreaming APIのようなものがない(多分)
  * XMPPでリアルタイム通知はある(loadaverage.orgは無効になってしまった)のでこれを有効にしたい
* 日本語翻訳がダメだったり追いついていない(未訳はともかく間違ってるとことか)
  * transifexで翻訳できる -> [GNU social | 概要](https://www.transifex.com/gnu-social/gnu-social/dashboard/ "GNU social | 概要")
* sourceも結構手を入れたいとこが……(password最低文字数が6なので増やしたいな……ハードコーディング!?/投稿文字数を無制限(設定項目0)にしてみよう->`Users can post short (0 character) status messages` )
* ちょこちょこやっていこうかと

---

# [Diaspora](https://diasporafoundation.org/)?知らない子ですね

* 今度試す……

---

# Mastodonが話題になってまだ1月だけどなんだか世界線が変わったような気分……．

![200%](G+01.jpg)

---

powered by [Marp - Markdown Presentation Writer](https://yhatt.github.io/marp/ "Marp - Markdown Presentation Writer")
Licence : [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
