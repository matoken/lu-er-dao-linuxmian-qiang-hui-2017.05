<!-- Marp(Markdown Presentation Writer)向け
     https://yhatt.github.io/marp/ -->
<!-- $theme: default -->
<!-- page_number: true -->
<!-- $size: 4:3 -->
<!-- footer: @matoken -->
<!-- prerender: true -->

# 鹿児島Linux勉強会 2017.05
## 日時 : 2017-05-13 14:00-17:00
## 会場 : [モノづくりスペース TUKUDDO](http://tukuddo.com/ "モノづくりスペース TUKUDDO")

---

# 鹿児島らぐ

* 鹿児島らぐのMLで開催調整やフォローアップなども行っています
  * [鹿児島らぐ https://kagolug.org/](https://kagolug.org/)
  * Twitter : [@kagolug](https://twitter.com/kagolug) / [@kagolug_ml](https://twitter.com/kagolug_ml)
  * [Lingr](http://lingr.com/signup?letmein=kagolug)??

*  <s>Mastodon:elephant::x:</s> GNU social:cow:もはじめました
  * [KagoLUG social](https://gnusocial.kagolug.org/)
  [@info](@info@gnusocial.kagolug.org) [@ml](@ml@gnusocial.kagolug.org)
MatodonやRSS Feed Readerで購読できます
MLメンバーはユーザー登録可能

* 色々お手伝い募集

---

# facebook?
# Slack?
# :
# ぜひ作って教えてください

---

![bg 80%](moccamaster-2.png)
# 会場について

* [モノづくりスペース TUKUDDO http://tukuddo.com/](http://tukuddo.com/ "モノづくりスペース TUKUDDO")
* 今日の会場費 : コーヒー1杯付き1000円
* 飲食 : OK
* 電源/Wi-Fiあり
  ESSID : 
  Passphrase : 

---

# Chromecast

* 

---

# 最近のイベント

---

# 鹿児島Linux勉強会2017.04(前回)

## 日時 : 2017-04-13(Sat) 14:00-17:00
## 会場 : サンエールかごしま 小研修室3

### 参加者は遠方からの参加者1名を含む5名でした

* 以下のような話題がありました．
Dockerコンテナ内のssh serverにアクセスできない / PQI Air Pen hack / OpenStreetMapで鹿児島市電の2軌道目を作成中 / Yumのエラーについての報告 / Chromeのリモートデスクトップで速度が出ない与太話 / CyanogenModはどこに消えた? / Windows10タブレットに各種Linuxディストリを入れて遊ぼう

---
![100% bg](kagolug_2017-04.jpg)

* [鹿児島Linux勉強会 2017.04 - connpass](https://kagolug.connpass.com/event/52477/ "鹿児島Linux勉強会 2017.04 - connpass")
  * [鹿児島Linux勉強会 2017.04 - 資料一覧 - connpass](https://kagolug.connpass.com/event/52477/presentation/ "鹿児島Linux勉強会 2017.04 - 資料一覧 - connpass")
* [Titanpad](https://kagolug.titanpad.com/ep/pad/view/10/latest)
* [鹿児島linux勉強会2017.04の記録 - Togetterまとめ](https://togetter.com/li/1102260 "鹿児島linux勉強会2017.04の記録 - Togetterまとめ")

---

# これからのイベント

* 2017-06-?? 鹿児島Linux勉強会2017.06
* 2017-07-08(Sat) 鹿児島Linux勉強会2017.07

---

## 発表

* アンカンファレンス形式で発表したいネタを持った人が発表します．出来るだけ全員発表を目指しましょう．  
* 発表ネタはLinux やPC-UNIX などにかすってればOK
* ちょっとした事や相談事とかもOK
* 相談がある人は具体的な機械の型番やOSやソフトウェアの情報，操作ログ等をメモしてくると幸せになるかもしれません．
* 解決しなかった場合はメーリングリストで続きをしましょう．
* 皆でメモを取りましょう -> goo.gl/TuqBgU 
<!-- ![](titanpad_qr.png) -->
